import { configureStore } from "@reduxjs/toolkit";
import productReducer from "./product/productReducer";

const store=configureStore({
    reducer:{
        products:productReducer,
    }
});
export default store;