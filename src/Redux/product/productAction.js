import {FETCH_USER_REQUEST,FETCH_USER_SUCCESS,FETCH_USER_FAILURE} from './productType'
import axios from 'axios'


export const fetchUserRequest=()=>{
    return {
        type:FETCH_USER_REQUEST
    }
}
export const fetchUserSuccess=(products)=>{
    return {
        type:FETCH_USER_SUCCESS,
        payload:products
    }
}
export const fetchUserFailure=()=>{
    return {
        type:FETCH_USER_FAILURE,
        payload:'error'
    }
}

export const fetchProducts=()=>{
    return (dispatch)=>{
        dispatch(fetchUserRequest)  
        axios.get('https://fakestoreapi.com/products')
        .then((response)=>{
            const products=response.data
            dispatch(fetchUserSuccess(products))
        })
        .catch((error)=>{
            const errorMessage=error.message
            dispatch(fetchUserFailure(errorMessage))
        })
    }

}