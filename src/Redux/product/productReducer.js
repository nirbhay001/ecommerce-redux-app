import {
  FETCH_USER_SUCCESS,
  FETCH_USER_FAILURE,
  FETCH_USER_REQUEST,
  ADD_DATA,
  REMOVE_DATA,
  UPDATE_DATA,
  ADD_CART,
  REMOVE_CART,
  UPDATE_PRODUCTS
} from "./productType";

const initialState = {
  loading: "loading",
  products: [],
  payload: "",
  cartProduct: [],
};

const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USER_REQUEST:
      return {
        ...state,
        loading: "loading",
      };
    case FETCH_USER_SUCCESS:
      return {
        ...state,
        loading: "loaded",
        products: action.payload,
      };
    case FETCH_USER_FAILURE:
      return {
        loading: "error",
        products: [],
      };
    case REMOVE_DATA:
      return {
        ...state,
        products: state.products.filter((item) => {
          return item.id !== action.payload;
        }),
      };
    case UPDATE_DATA:
      return {
        ...state,
        products: [...state.products.map((item) => {
          return item.id === action.payload.id ? action.payload : item;
        })]
      };
    case ADD_CART:
      if(state.cartProduct===undefined){
        return;
      }
      let itemIndex = state.cartProduct.findIndex(
        (item) => item.id === action.payload.id
      );
      if (itemIndex !== -1) {
        const updatedCartState = state.cartProduct.map((item, index) => {
          if (index === itemIndex) {
            return {
              ...item,
              quantity: item.quantity + 1,
              price: item.price + action.payload.price,
            };
          } else {
            return item;
          }
        });
        return {
          ...state,
          cartProduct: updatedCartState,
        };
      } else {
        const updatedCartState = [
          ...state.cartProduct,
          { ...action.payload, quantity: 1 },
        ];
        return {
          ...state,
          cartProduct: updatedCartState,
        };
      }
    case ADD_DATA:
      return {
        ...state,
        products: [...state.products, action.payload],
      };
    case REMOVE_CART:
      return {
        ...state,
        cartProduct: [
          ...state.cartProduct.filter((item) => {
            return item.id !== action.payload;
          }),
        ],
      };
    case UPDATE_PRODUCTS:
      return {
        ...state,
        cartProduct: action.payload,
      };
    default:
      return state;
  }
};

export default productReducer;
