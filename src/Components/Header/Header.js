import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./Header.css";
import { connect } from "react-redux";

export class Header extends Component {
  render() {
    return (
      <div className="header">
        <span>
          <Link to="/">
            <i className="fa-solid fa-house cart-icon"></i>
          </Link>
        </span>
        <span className="product-type">
          <Link to="/">
            <span><i className="fa-solid fa-truck-arrow-right"></i>E Cart</span>
          </Link>
        </span>
        <div className="add-product">
          <Link to="/addItem">
            <button className="add-button">Add Item</button>
          </Link>
          <span className="cart-icon">
           <Link to="/cartItem">
            <i className="fa-solid fa-cart-shopping .bg-dark"></i>
            <span className="cartItem">{this.props.cartLength}</span>
            </Link> 
          </span>
        </div>
      </div>
    );
  }
}

const mapStateToProps=state=>{
  return {
    cartLength:state.products.cartProduct.length
  }
}

export default connect(mapStateToProps)(Header);
