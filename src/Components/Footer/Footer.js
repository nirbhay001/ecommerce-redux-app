import React, {Component} from "react";
import "./Footer.css";

class Footer extends Component {
    render(){
  return (
    <div className="footer-container">
      <div className="footer">
        <div>
          All copyright reserved <span>&#169;</span>
        </div>
      </div>
    </div>
  );
    }
}

export default Footer;