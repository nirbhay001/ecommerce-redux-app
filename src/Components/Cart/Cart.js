import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./Cart.css";
import { connect } from "react-redux";


export class Cart extends Component {


  constructor(props){
    super(props);
  }


  itemPlus = (product) => {
    if (product.quantity >= 5) {
      return;
    }

    this.props.setCartState(
      this.props.cartState.map((cartItem) => {
        if (cartItem.id === product.id) {
          return {
            ...cartItem,
            quantity: cartItem.quantity + 1,
            price:
              Number(cartItem.price) +
              Number(cartItem.price) / cartItem.quantity,
          };
        } else {
          return cartItem;
        }
      }),
    );
  };

  itemMinus = (product) => {
    if (product.quantity <= 1) {
      return;
    }
    this.props.setCartState(
      this.props.cartState.map((cartItem) => {
        if (cartItem.id === product.id) {
          return {
            ...cartItem,
            quantity: cartItem.quantity - 1,
            price:
              Number(cartItem.price) -
              Number(cartItem.price) / cartItem.quantity,
          };
        } else {
          return cartItem;
        }
      }),
    );
  };



  render() {
    let total=0;
    return (
      <div >
        <Link to="/">
          <button className="btn btn-success m-3">Back to Home</button>
        </Link>
        {this.props.cartState !== undefined
          ? this.props.cartState.map((cartItem) => {
              total = total + Number(cartItem.price);
              return (
                <div className="cart cart-style" key={cartItem.id}>
                  <img
                    src={cartItem.image}
                    className="cart-image"
                    alt="image of product"
                  ></img>
                  <span className="description">{cartItem.description}</span>
                  <span className="cartItem-price">
                    {Number(cartItem.price).toFixed(2)}$
                  </span>
                  <span className="button-item">
                    <button
                      className="button-style"
                      onClick={() => this.itemPlus(cartItem)}
                    >
                      +
                    </button>
                    <div className="cartItem-quantity">{cartItem.quantity}</div>
                    <button
                      className="button-style"
                      onClick={() => this.itemMinus(cartItem)}
                    >
                      -
                    </button>
                  </span>
                  <button
                    className="bg-danger text-white"
                    onClick={() => this.props.removeItem(cartItem.id)}
                  >
                    Remove Item
                  </button>
                </div>
              );
            })
          : ""}
        <div className="total-price">
          <div>Total Price</div>
          <div className="totalPrice">{total.toFixed(2)}</div>
        </div>
      </div>
    );
  }
}

const mapStateToProps=state=>{
    return {
        cartState:state.products.cartProduct,
    }
}

const mapDispatchToProps = (dispatch) => {
  return {
    removeItem: (id) => dispatch({
      type:'REMOVE_CART',
      payload:id
    }),
    setCartState: (products) => dispatch({
      type:'UPDATE_PRODUCTS',
      payload:products
    }),
  };
};
export default connect(mapStateToProps,mapDispatchToProps)(Cart);
