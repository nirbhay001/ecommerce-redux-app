import React, { Component } from "react";
import { connect } from "react-redux";
import "./EditItem.css";

class EditItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: null,
      editFlag: false,
    };
  }

  getProduct = (id) => {
    if(this.props.productsDetails===undefined){
      this.setState({
        ...this.state.product,
        product: null
      });
      return;
    }
    const productData = this.props.productsDetails.find(
      (currentProduct) => {
        return currentProduct.id.toString() === id.toString();
      }
    );

    this.setState({
      ...this.state.product,
      product: productData
    });
  };

  componentDidMount() {
    const id = window.location.href.split("/").slice(4);
    this.getProduct(id);
  }

  handleUpdateChange = (event) => {
    let name = event.target.name;
    let value = event.target.value;
    this.setState((prevState) => ({
      product: { ...prevState.product, [name]: value },
    }));
  };

  editHandleSubmit = (event) => {
    event.preventDefault();
    let passData=this.state.product
    this.props.handleProductUpdate(passData);
    this.setState((prevState) => ({
      editFlag: { ...prevState, dataFlag: true },
    }));
  };

  render() {
    return this.state.product === null || this.state.product=== undefined ? (
      <h1 className="edit-message">Go to home page</h1>
    ) : (
      <div className="editItem">
        <form onSubmit={this.editHandleSubmit}>
          <div className="form-group">
            <label htmlFor="title">Title</label>
            <input
              type="text"
              className="form-control mt-1"
              name="title"
              placeholder="Enter title"
              value={this.state.product.title}
              onChange={(event) => this.handleUpdateChange(event)}
              required
            />
          </div>
          <div className="form-group mt-3">
            <label htmlFor="category">Category</label>
            <input
              type="text"
              className="form-control mt-1"
              name="category"
              placeholder="Enter category"
              value={this.state.product.category}
              required
              onChange={this.handleUpdateChange}
            />
          </div>
          <div className="form-group mt-3">
            <label htmlFor="price">Price</label>
            <input
              type="number"
              className="form-control mt-1"
              name="price"
              placeholder="Enter price"
              value={this.state.product.price}
              required
              onChange={this.handleUpdateChange}
            />
          </div>
          <div className="form-group mt-3">
            <label htmlFor="imageurl">Image URL</label>
            <input
              type="text"
              className="form-control mt-1"
              name="image"
              placeholder="Enter image URL"
              value={this.state.product.image}
              required
              onChange={this.handleUpdateChange}
            />
          </div>
          <button type="submit" className="btn btn-primary mt-4">
            Add Item
          </button>
          {this.state.editFlag ? <p>Data updated succesfully</p> : ""}
        </form>
      </div>
    );
  }
}

const mapStateToProps=state=>{
  return{  
    productsDetails:state.products.products,
    status:state.products.loading
  }
}

const mapDispatchToProps = (dispatch)=>{
  return { 
    handleProductUpdate: (productData) => dispatch({
      type: 'UPDATE_DATA',
      payload: productData
    })
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(EditItem);
