import React, { Component } from "react";
import Card from "../Card/Card";
import { InfinitySpin } from "react-loader-spinner";
import "./Products.css";
import { connect } from "react-redux";

class Products extends Component {
  render() {
    return (
      <>
        {this.props.API_STATES == 'loading' ? (
          <div className="spinner">
            <InfinitySpin width="200" color="#4fa94d" />
          </div>
        ) : this.props.API_STATES==="error"? (
          <h1 className="data-flag">Error occured in fetching the data </h1>
        ) :this.props.productsData.length === 0? (
          <h1 className="data-flag">Data not Found</h1>
        ) : (
          <div className="products">
            {this.props.productsData.map((product) => {
              return (
                <Card
                  product={product}
                  key={product.id}
                />
              );
            })}
          </div>
        )}
      </>
    );
  }
}

const mapStateToProps=state=>{
  return{
    productsData:state.products.products,
    API_STATES:state.products.loading
  }
}

export default connect(mapStateToProps,null) (Products);
