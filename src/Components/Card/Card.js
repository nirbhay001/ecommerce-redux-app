import React, { Component } from "react";
import "./Card.css";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class Card extends Component {
  render() {
    const { product} = this.props;
    return (
      <div className="card" key={product.id}>
        <div className="icon-status">
          <Link to={`editPage/${product.id}`}>
            <span>
              <i className="fas fa-edit"></i>Edit
            </span>
          </Link>
          <span onClick={() => this.props.handleDelete(product.id)}>
            <i className="fa-solid fa-trash"></i>delete
          </span>
        </div>
       <img src={product.image} alt="product-image" className="card-image" />
        <div>
          Price<span className="text-bold"> : {product.price}$</span>
        </div>
        <div>{product.title}</div>
        <div>
          Rating<span className="text-bold"> : {product.rating.rate}</span>
        </div>
        <div>
          Category<span className="text-bold"> : {product.category}</span>
        </div>
        <div>
          Count<span className="text-bold"> : {product.rating.count}</span>
        </div>
        <div>
          <button  className="btn btn-primary" onClick={()=>this.props.addToCart(product)}>Add to cart</button>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch =>{
  return {
   handleDelete:(id)=>dispatch({
      type:'REMOVE_DATA',
      payload:id
    }),
    addToCart:(product)=>dispatch({
      type:'ADD_CART',
      payload:product
    })
  }
}


export default connect(null,mapDispatchToProps)(Card);
