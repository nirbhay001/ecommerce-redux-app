import "./App.css";
import React, { Component } from "react";
import Header from "./Components/Header/Header";
import Products from "./Components/Products/Products";
import AddItem from "./Components/AddItem/AddItem";
import Footer from "./Components/Footer/Footer";
import { Routes, Route } from "react-router-dom";
import EditItem from "./Components/EditItem/EditItem";
import { fetchProducts } from "./Redux/product/productAction";
import { connect } from "react-redux";
import Cart from "./Components/Cart/Cart";


class App extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.fetchProducts();
  }

  render() {
    return (
      <div>
        <Header />
        <Routes>
          <Route path="/" element={<Products />}></Route>
          <Route path="editPage/:productId" element={<EditItem />}></Route>
          <Route path="additem" element={<AddItem />}></Route>
          <Route path="cartItem" element={<Cart />}></Route>
        </Routes>
        <Footer/>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchProducts: () => dispatch(fetchProducts()),
  };
};

export default connect(null, mapDispatchToProps)(App);
